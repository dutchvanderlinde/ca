const Discord = require("discord.js");
const bot = new Discord.Client();
const fs = require('fs');
const ids = require('./utils/ids.json')
const tokenfile = require('./gay/token.json');
require('./events/handler')(bot)

bot.commands = new Discord.Collection();
bot.aliases = new Discord.Collection();


// Now Comes the command handler 
const loadCommands = module.exports.loadCommands = (dir = "./commands/") => {
    fs.readdir(dir, (error, files) => {                   // Reading the Dir
        if (error) {
            require('../utils/err.js')
        } 
                           
        files.forEach((file) => {                       // reading Files from each dir
            if (fs.lstatSync(dir + file).isDirectory()) {
                loadCommands(dir + file + "/");
                return;
            }

            delete require.cache[require.resolve(`${dir}${file}`)];

            let props = require(`${dir}${file}`); // defining props for each file for each dir

            bot.commands.set(props.command.name, props); // giving name to the command

            if (props.command.aliases)  props.command.aliases.forEach(alias => { 
                bot.aliases.set(alias, props.command.name); // giving aliases to the command [second name]
            });
        });
    });
};
loadCommands();

bot.on('message', message => {
    let prefix = ">"
    let args = message.content.slice(prefix.length).trim().split(/ +/g);
    let cmd = args.shift().toLowerCase();
    let command;
    
    if (bot.commands.has(cmd)) {
        command = bot.commands.get(cmd);
    } else if (bot.aliases.has(cmd)) {
        command = bot.commands.get(bot.aliases.get(cmd));
    }

    if (!message.content.startsWith(prefix)) return;

    if (command) {
        // The Below line will check if the command is enabled or else it will give a message that command is disabled and the user cannot use it 
        if (message.author.id !== ids.joe && !command.command.enabled) return;
    }
    try {
        command.run(bot, message, args);

    } catch (e) {
        console.log(e)
    }
})

bot.on("guildMemberAdd", member => {
    let welcomechannel = member.guild.channels.find(c => c.name === "♡-lobby-♡");
    if(!welcomechannel) return;
      welcomechannel.send(`**Welcome to Chino's Café, <@${member.user.id}>! Enjoy your stay. Make sure to read <#408922399459901452> & <#513079626684301322> and have a :cookie::hearts:!**`);
});
bot.login(tokenfile.token);