module.exports = async bot => {
    bot.user.setActivity("Reconnecting.", {
        type: "WATCHING"
    });
    const {
        get
    } = require("snekfetch");
    await get(`http://artii.herokuapp.com/make?text=CA++RECONNECTING&font=big`).then(res => {
       console.log(res.body.toString())
    });
}