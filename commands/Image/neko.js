const Discord = require("discord.js");
const { get } = require("snekfetch");
module.exports.run = async (bot, message, args) => { 
    try {
 get("https://nekos.life/api/neko").then(res => {
            const embed = new Discord.RichEmbed()
            .setImage(res.body.neko)
            .setColor("0xFFB6C1")
            .setAuthor(`Neko~`)

            setTimeout(() => {
                return  message.channel.send({embed});
            }, 100);
        });
    } catch(err) {
        console.log(err);
    }
}
module.exports.command = {
    name: 'neko',
    aliases: ["nko"],
    permission: "",
    description: "Sends a random Neko pic.",
    usage: ">neko",
    category: "Miscellaneous",
    enabled: true
};