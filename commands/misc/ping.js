module.exports.run = async (bot, message, args) => {
    let start = Date.now();
    let messagesent = await message.channel.send('hong!');
    messagesent.edit(`hong! \`${Date.now() - start}ms\``);
}
module.exports.command = {
    name: 'ping',
    aliases: ["pong", "pint", "piny", "mm"],
    permission: "",
    description: "Ping the bot",
    usage: ">ping",
    category: "Miscellaneous",
    enabled: true
};