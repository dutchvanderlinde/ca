module.exports.run = async (bot, message, args) => {
    if (message.author.id === "208688963936845824" || message.author.id === "233667448887312385") { 
        const clean = text => {
            if (typeof(text) === "string")
              return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
            else
                return text;
          }
        try {
            const code = args.join(" ");
            let evaled = eval(code);

            if (typeof evaled !== "string")
                evaled = require("util").inspect(evaled);

            message.channel.send(clean(evaled), {
                code: "js"
            });
        } catch (err) {
            message.channel.send(`\`\`\`js\n${clean(err)}\n\`\`\``);
        }
    }
}
module.exports.command = {
    name: 'e',
    aliases: ["eval", "ev"],
    permission: "",
    description: "Evaluate code",
    usage: ">eval <code>",
    category: "Owner",
    enabled: true
};