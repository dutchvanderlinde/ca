const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
  if(message.author.id != "208688963936845824" && message.author.id != "267442453542469633" && message.author.id != "489003898783399937" && message.author.id != "189828644019896321" && message.author.id != "255395378885689344") return;

message.delete()

 let embed1 = new Discord.RichEmbed()
  .setTitle("Welcome to Chino's Café!", "https://www.youtube.com/channel/UCaVzcs_eSVXPkmKoNlZuknQ")
  .setThumbnail(message.guild.iconURL)
  .setImage("https://cdn.discordapp.com/attachments/522724908187385856/522889246256660490/IMG_20181213_233443.png")
  .setDescription("<:KotoWave:495502862735114240> Welcome to Chino's Café!\n\nMake sure to read <#408922399459901452> and the information below, if you have any questions feel free to ask. Have fun :hearts: ")
  .setColor("0xFFB6C1")
  message.channel.send(embed1);
  
  // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
  let embed2 = new Discord.RichEmbed()
  .setTitle("INFORMATION")
  .setColor("0xFFB6C1")
  .setDescription("**> What is Chino's Café ?**\n Chino's Café is a community server for the Nightcore YouTuber **[Chino](https://www.youtube.com/channel/UCaVzcs_eSVXPkmKoNlZuknQ)**, events, giveaways, activity rewards and much more all in one place.\n\n**> Who is Chino and is she active?**\n<@189828644019896321> Is Chino and yes she is active in the server.\n\n**> These colors, I want <:yes:401123060876312595>**\nTo know more about all the roles and how to obtain the, read the message below hearts")
  message.channel.send(embed2);

  // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
  let embed3 = new Discord.RichEmbed()
  .setTitle("ROLES")
  .setColor("0xFFB6C1")
  .setDescription("- **The Roles of The Server!** -\n\n<@&300648088312479744> - Owner | Chino herself.\n<@&332120961547501582> - Server's management. (Highest staff rank attainable)\n<@&466919391246942218> - Help run the server and staff. (2nd highest staff rank)\n<@&300649064947646465> - Server Official Moderators.\n<@&325750928076701699> - Server Official Helpers.\n<@&446761484656050176> - Server's Official Trial-Mods.\n<@&332194156804046850> - Those who lost their permissions to text due to rules breaking.\n<@&345914403004088322> - Friends of Chino (Don't ask for it)\n<@&300656667207532565> - Known YouTubers (Don't ask for it)\n<@&347376148411449344> - Productive artists (Don't ask for it)\n<@&414611255106600960> - The server bots.\n<@&426189901902381057> - Mentionable role to notify users about events (You can get rid of it by doing `?events` in <#513079980079448065>)\n<@&421370851120775169> - Special colored role (Don't ask for it)\n<@&332533288327577600> - Cute colored role (`.iam cute` in <#513079980079448065>)\n\nThe rest are either level-up ranks/Color roles/Temporary roles.")
  message.channel.send(embed3);
  
  // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
  let embed4 = new Discord.RichEmbed()
  .setTitle("ACTIVITY REWARDS")
  .setColor("0xFFB6C1")
  .setDescription("**- Activity Roles -**\n\n**•**<@&520717389302595585> - 100,000XP\n**•**<<@&520717394369314825> - 76,000XP\n**•**<@&514970134968795159> - 58,000XP\n**•**<@&514969903082635284> - 43,000XP\n**•**<@&514969900746276929> - 32,000XP\n**•**<@&514969890910896129> - 22,000XP\n**•**<<@&520717398999957509> - 13,000XP\n**•**<@&520717402946928640> - 6,000XP\n**•**<@&520717406017028096> - 1,000XP\n\nThe more active you are the more prizes you can get, we will also have a shop system where you can buy stuff with the \"coins\" you get from just being active here, meaning the more active you are the more coins you get, the more coins you get the better prize you get.(Prizes range from a colored role all the way to Nitro/Games so.. Keep up)")
  message.channel.send(embed4);
  // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function listppl(x,y,z) {
let x1 = message.guild.roles.get(x).members.map(m=>m.user.id);
let x2 = x1.map(id => `<@${id}>`);
let x3 = x2.join(', ');

let y1 = message.guild.roles.get(y).members.map(m=>m.user.id);
let y2 = y1.map(id => `<@${id}>`);
let y3 = y2.join(', ');

let z1 = message.guild.roles.get(z).members.map(m=>m.user.id);
let z2 = z1.map(id => `<@${id}>`);
let z3 = z2.join(', ');

let embed5 = new Discord.RichEmbed()
  .setAuthor('Chino Staff')
  .setColor("0xFFB6C1")
  .setDescription(`**• __Staff List__ •**\n\n**Owner: <@189828644019896321>**.\n**Admins:** ${x3}.\n**Head of Staff:** ${y3}.\n**Chino Police:** ${z3}.`);
  message.channel.send(embed5)
}
listppl('332120961547501582','466919391246942218','521563866375913482');
}
module.exports.command = {
    name: 'welcome',
    aliases: ["wlc", "wlcmsg", "welcomemsg"],
    permission: "",
    description: "Sends Chino's welcome message.",
    usage: ">welcome",
    category: "Owner",
    enabled: true
};